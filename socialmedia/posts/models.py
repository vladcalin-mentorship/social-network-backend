import secrets
from os.path import splitext

from django.contrib.auth import get_user_model
from django.db import models


def get_image_path(instance, filename):
    _, ext = splitext(filename)
    return f'posts/{secrets.token_urlsafe(20)}{ext}'


# Create your models here.
class Post(models.Model):
    posted_by_user = models.ForeignKey(get_user_model(), on_delete=models.SET_NULL, null=True, related_name='posts')
    posted_by_page = models.ForeignKey('pages.Page', on_delete=models.SET_NULL, null=True, related_name='posts')

    content = models.TextField(blank=True)
    date_posted = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=get_image_path, null=True, blank=True)
