import pytest

from posts.models import Post


@pytest.mark.django_db
def test_create_post_as_user(auth_client, django_user):
    resp = auth_client.post('/api/v1/posts/', {'posted_by_user': django_user.id, 'content': 'Hello world'})
    assert resp.status_code == 201

    resp_json = resp.json()
    for k, v in {
        'id': 1,
        'content': 'Hello world',
        'image': None,
        'posted_by_user': django_user.id,
        'posted_by_page': None,
    }.items():
        assert resp_json[k] == v

    assert 'date_posted' in resp_json


@pytest.mark.django_db
def test_create_post_as_wrong_user(auth_client, other_user):
    resp = auth_client.post('/api/v1/posts/', {'posted_by_user': other_user.id, 'content': 'Hi'})
    assert resp.status_code == 400


@pytest.mark.django_db
def test_create_post_as_page(auth_client, django_user, pages):
    resp = auth_client.post('/api/v1/posts/', {'posted_by_page': pages[0].id, 'content': 'Hi'})
    assert resp.status_code == 201


@pytest.mark.django_db
def test_create_post_as_wrong_page(auth_client, django_user, pages):
    resp = auth_client.post('/api/v1/posts/', {'posted_by_page': pages[1].id, 'content': 'Hi'})
    assert resp.status_code == 400


@pytest.mark.django_db
def test_update_post_as_user(auth_client, django_user):
    p = Post.objects.create(posted_by_user=django_user, content='test')
    resp = auth_client.patch(f'/api/v1/posts/{p.id}/', {'content': 'blah'})
    assert resp.status_code == 200
    p.refresh_from_db()
    assert p.content == 'blah'


@pytest.mark.django_db
def test_update_post_as_wrong_user(auth_client, django_user, other_user):
    p = Post.objects.create(posted_by_user=other_user, content='test')
    resp = auth_client.patch(f'/api/v1/posts/{p.id}/', {'content': 'blah'})
    assert resp.status_code == 403


@pytest.mark.django_db
def test_update_post_as_page(auth_client, django_user, pages):
    p = Post.objects.create(posted_by_page=pages[0], content='test')
    resp = auth_client.patch(f'/api/v1/posts/{p.id}/', {'content': 'blah'})
    assert resp.status_code == 200
    p.refresh_from_db()
    assert p.content == 'blah'


@pytest.mark.django_db
def test_update_post_as_wrong_page(auth_client, django_user, pages):
    p = Post.objects.create(posted_by_page=pages[1], content='test')
    resp = auth_client.patch(f'/api/v1/posts/{p.id}/', {'content': 'blah'})
    assert resp.status_code == 403


@pytest.mark.django_db
def test_delete_post_as_user(auth_client, django_user, pages):
    p = Post.objects.create(posted_by_page=pages[0], content='test')
    resp = auth_client.delete(f'/api/v1/posts/{p.id}/')
    assert resp.status_code == 204
    assert not Post.objects.filter(pk=p.id).exists()


@pytest.mark.django_db
def test_delete_post_as_wrong_user(auth_client, django_user, pages):
    p = Post.objects.create(posted_by_page=pages[1], content='test')
    resp = auth_client.delete(f'/api/v1/posts/{p.id}/')
    assert resp.status_code == 403
    assert Post.objects.filter(pk=p.id).exists()
