from django.core.exceptions import ValidationError
from rest_framework import serializers

from users.models import User, FriendRequest


class PublicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'avatar')


class FullUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
    )

    def save(self, **kwargs):
        instance = super(FullUserSerializer, self).save(**kwargs)
        if 'password' in kwargs:
            instance.set_password(kwargs['password'])
            instance.save()
        return instance

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'password', 'email', 'date_joined', 'username', 'avatar')
        read_only_fields = ('id', 'date_joined')


class CurrentUserSerializer(FullUserSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False
    )
    email = serializers.EmailField(required=True)


class FriendRequestSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        if FriendRequest.objects.filter(from_user=self.context['request'].user, to_user=attrs['to_user']).exists():
            raise ValidationError('Friend request already sent')
        return attrs

    def save(self, **kwargs):
        kwargs['from_user'] = self.context['request'].user
        return super(FriendRequestSerializer, self).save(**kwargs)

    class Meta:
        model = FriendRequest
        fields = ('id', 'to_user', 'date')


class DetailedFriendRequestSerializer(serializers.ModelSerializer):
    to_user = PublicUserSerializer()

    class Meta:
        model = FriendRequest
        fields = ('id', 'to_user', 'date')
