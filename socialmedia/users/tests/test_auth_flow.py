import pytest


@pytest.mark.django_db
def test_auth_flow(api_client, django_user):
    django_user.set_password('P@ssw0rd')
    django_user.save()
    resp = api_client.post('/api/v1/login/', {'username': 'test', 'password': 'P@ssw0rd'})
    token = resp.json()['token']

    resp = api_client.get('/api/v1/users/me/', HTTP_AUTHORIZATION=f'Token {token}')
    assert resp.status_code == 200
    assert resp.json()['email'] == 'test@example.com'
