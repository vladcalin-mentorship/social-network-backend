from django.core.exceptions import ValidationError
from rest_framework import serializers

from pages.models import Page
from posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        current_user = self.context['request'].user
        if attrs.get('posted_by_user'):
            if current_user != attrs['posted_by_user']:
                raise ValidationError('Can not post as another user')
        elif attrs.get('posted_by_page'):
            if attrs['posted_by_page'].created_by != current_user:
                raise ValidationError('Can not post as an unowned page')
        else:
            raise ValidationError('At least one of posted_by_user or posted_by_page must be passed.')
        return attrs

    class Meta:
        model = Post
        fields = ('id', 'posted_by_user', 'posted_by_page', 'content', 'date_posted', 'image')


class PostUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'posted_by_user', 'posted_by_page', 'content', 'date_posted', 'image')
        read_only_fields = ('id', 'posted_by_user', 'posted_by_page', 'date_posted')
