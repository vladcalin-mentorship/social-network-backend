import pytest


@pytest.mark.django_db
def test_anonymous_me(client, django_user):
    resp = client.get('/api/v1/users/me/')
    assert resp.status_code == 401


@pytest.mark.django_db
def test_authenticated_me(auth_client):
    resp = auth_client.get('/api/v1/users/me/')
    assert resp.status_code == 200
    assert resp.json()['username'] == 'test'
    assert resp.json()['email'] == 'test@example.com'
    assert resp.json()['first_name'] == 'John'
    assert resp.json()['last_name'] == 'Doe'


@pytest.mark.django_db
def test_authenticated_update_me(auth_client, django_user):
    assert django_user.first_name == 'John'
    resp = auth_client.patch('/api/v1/users/me/', {'first_name': 'Vlad'})
    assert resp.status_code == 200
    django_user.refresh_from_db()
    assert django_user.first_name == 'Vlad'


@pytest.mark.django_db
def test_anonymous_update_me(api_client, django_user):
    resp = api_client.patch('/api/v1/users/me/', {'first_name': 'Vlad'})
    assert resp.status_code == 401
    django_user.refresh_from_db()
    assert django_user.first_name == 'John'


@pytest.mark.django_db
def test_anonymous_delete_me(api_client, django_user):
    resp = api_client.delete('/api/v1/users/me/')
    assert resp.status_code == 401


@pytest.mark.django_db
def test_authenticated_delete_me(auth_client, django_user):
    resp = auth_client.delete('/api/v1/users/me/')
    assert resp.status_code == 405
    # it should succeed, because the user still needs to be in the database
    django_user.refresh_from_db()
