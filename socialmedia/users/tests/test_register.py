import pytest

from users.models import User


@pytest.mark.django_db
def test_register(api_client):
    resp = api_client.post('/api/v1/users/register/', {
        'username': 'vladcalin',
        'first_name': 'Vlad',
        'last_name': 'Calin',
        'password': 'P@ssw0rd',
        'email': 'vlad.calin@example.com'
    })
    assert resp.status_code == 201
    assert User.objects.get(email='vlad.calin@example.com')


@pytest.mark.django_db
def test_register_email_and_username_taken(api_client, django_user):
    resp = api_client.post('/api/v1/users/register/', {
        'username': 'test3',
        'first_name': 'X',
        'last_name': 'Y',
        'password': 'P@ssw0rd',
        'email': 'test@example.com'
    })
    assert resp.status_code == 400
    assert resp.json()['email'] == ['user with this email already exists.']

    resp = api_client.post('/api/v1/users/register/', {
        'username': 'test',
        'first_name': 'X',
        'last_name': 'Y',
        'password': 'P@ssw0rd',
        'email': 'allgood@example.com'
    })
    assert resp.status_code == 400
    assert resp.json()['username'] == ['A user with that username already exists.']


