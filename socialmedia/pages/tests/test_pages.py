import pytest


@pytest.mark.django_db
def test_list_pages_anonymous(api_client, pages):
    resp = api_client.get('/api/v1/pages/')
    assert resp.status_code == 200
    assert resp.json()['count'] == 2
    assert resp.json()['results'] == [
        {
            'id': 1,
            'name': 'Page 1',
            'image': None,
            'cover': None,
            'description': '',
            'created_by': {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'avatar': None
            },
            'type': ''
        },
        {
            'id': 2,
            'name': 'Page 2',
            'image': None,
            'cover': None,
            'description': '',
            'created_by': {
                'id': 2,
                'first_name': 'Jane',
                'last_name': 'Doe',
                'avatar': None
            },
            'type': ''}
    ]


@pytest.mark.django_db
def test_list_pages_authenticated(auth_client, pages, django_user, other_user):
    resp = auth_client.get('/api/v1/pages/')
    assert resp.status_code == 200
    assert resp.json()['count'] == 2

    def datestr(d):
        return d.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    assert resp.json()['results'] == [
        {
            'id': 1,
            'name': 'Page 1',
            'image': None,
            'cover': None,
            'description': '',
            'created_by': {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'date_joined': datestr(django_user.date_joined),
                'email': 'test@example.com',
                'avatar': None,
                'username': 'test',
            },
            'date_created': datestr(pages[0].date_created),
            'date_updated': datestr(pages[0].date_updated),
            'type': ''
        },
        {
            'id': 2,
            'name': 'Page 2',
            'image': None,
            'cover': None,
            'description': '',
            'created_by': {
                'id': 2,
                'date_joined': datestr(other_user.date_joined),
                'first_name': 'Jane',
                'last_name': 'Doe',
                'email': 'test2@example.com',
                'username': 'test2',
                'avatar': None
            },
            'date_created': datestr(pages[1].date_created),
            'date_updated': datestr(pages[1].date_updated),
            'type': ''
        }
    ]
