import pytest

from users.models import User, FriendRequest


@pytest.mark.django_db
def test_anonymous_user_cannot_send_friend_request(client, django_user, other_user):
    resp = client.post('/api/v1/users/me/friend_requests/', {'to_user': other_user.id})
    assert resp.status_code == 401


@pytest.mark.django_db
def test_user_can_send_friend_request(auth_client, django_user, other_user):
    resp = auth_client.post('/api/v1/users/me/friend_requests/', {'to_user': other_user.id})
    assert resp.status_code == 201
    assert other_user.received_friend_requests.filter(from_user=django_user).count() == 1

    # can not send two
    resp = auth_client.post('/api/v1/users/me/friend_requests/', {'to_user': other_user.id})
    assert resp.status_code == 400
    assert other_user.received_friend_requests.filter(from_user=django_user).count() == 1


@pytest.mark.django_db
def test_user_can_revoke_friend_request(auth_client, django_user, other_user):
    resp = auth_client.post('/api/v1/users/me/friend_requests/', {'to_user': other_user.id})
    assert resp.status_code == 201
    friend_request = other_user.received_friend_requests.filter(from_user=django_user).first()

    # can revoke
    resp = auth_client.delete(f'/api/v1/users/me/friend_requests/{friend_request.id}/')
    assert resp.status_code == 204
    assert other_user.received_friend_requests.filter(from_user=django_user).count() == 0
    # can not edit, method not allowed
    resp = auth_client.patch(f'/api/v1/users/me/friend_requests/{friend_request.id}/', {'from_user': 1})
    assert resp.status_code == 405


@pytest.mark.django_db
def test_user_sees_only_its_own_friend_requests(auth_client, django_user, other_user):
    third_user = User.objects.create(email='test3@example.com', username='test3')
    fourth_user = User.objects.create(email='test4@example.com', username='test4')

    FriendRequest.objects.create(from_user=django_user, to_user=other_user)
    FriendRequest.objects.create(from_user=django_user, to_user=fourth_user)
    FriendRequest.objects.create(from_user=other_user, to_user=third_user)
    FriendRequest.objects.create(from_user=other_user, to_user=fourth_user)
    FriendRequest.objects.create(from_user=third_user, to_user=fourth_user)

    resp = auth_client.get('/api/v1/users/me/friend_requests/')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['count'] == 2
