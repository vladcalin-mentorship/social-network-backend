import pytest


@pytest.mark.django_db
def test_anonymous_list_public_users(client, django_user, other_user):
    resp = client.get('/api/v1/users/public/')
    assert resp.status_code == 200
    assert resp.json() == {
        'count': 2,
        'next': None,
        'previous': None,
        'results': [
            {'id': 1, 'first_name': 'John', 'last_name': 'Doe', 'avatar': None},
            {'id': 2, 'first_name': 'Jane', 'last_name': 'Doe', 'avatar': None},
        ]
    }


@pytest.mark.django_db
def test_anonymous_retrieve_public_user(client, django_user):
    resp = client.get('/api/v1/users/public/1/')
    assert resp.status_code == 200
    assert resp.json() == {'id': 1, 'first_name': 'John', 'last_name': 'Doe', 'avatar': None}


@pytest.mark.django_db
def test_anonymous_retrieve_non_existent_user(client, django_user):
    resp = client.get('/api/v1/users/public/5/')
    assert resp.status_code == 404
