from rest_framework.routers import DefaultRouter

from pages.viewsets import PageViewSet

router = DefaultRouter()
router.register('', PageViewSet, basename='pages')

urlpatterns = router.urls
