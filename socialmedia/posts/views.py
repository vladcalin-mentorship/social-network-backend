from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

# Create your views here.
from posts.models import Post
from posts.permissions import PostOwnerOrReadOnly
from posts.serializers import PostSerializer, PostUpdateSerializer


class ListCreatePostApiView(ListCreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (PostOwnerOrReadOnly,)

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Post.objects.none()

        return Post.objects.all().get_for_users(
            self.request.user.friends()
        ).get_for_pages(
            self.request.user.pages_liked()
        )


class RetrieveUpdateDestroyPostAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = PostUpdateSerializer
    permission_classes = (PostOwnerOrReadOnly,)
    queryset = Post.objects.all()
