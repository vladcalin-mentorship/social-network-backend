import secrets
from os.path import splitext

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


def get_user_avatar_path(instance, filename):
    _, ext = splitext(filename)
    return f'users/{instance.username}/{secrets.token_urlsafe(20)}{ext}'


class UserQuerySet(models.QuerySet):
    def friends(self):
        return User.objects.none()

    def liked_pages(self):
        from pages.models import Page
        return Page.objects.none()


class CustomUserManager(UserManager.from_queryset(UserQuerySet)):
    pass


class User(AbstractUser):
    avatar = models.ImageField(upload_to=get_user_avatar_path, null=True, blank=True)
    email = models.EmailField(unique=True)

    objects = CustomUserManager()


class FriendRequest(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_friend_requests')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_friend_requests')
    date = models.DateTimeField(auto_now_add=True)
