start:
	python socialmedia/manage.py makemigrations
	python socialmedia/manage.py migrate
	python socialmedia/manage.py runserver 0.0.0.0:8000

start.docker:
	docker run -it --rm --name social-network-backend -p 8000:8000 registry.gitlab.com/vladcalin-mentorship/social-network-backend

docker_build:
	docker build -t registry.gitlab.com/vladcalin-mentorship/social-network-backend .

docker_push:
	docker push registry.gitlab.com/vladcalin-mentorship/social-network-backend
