import functools

import pytest
from rest_framework.authtoken.models import Token

from pages.models import Page
from users.models import User


class ApiClient:
    def __init__(self, client):
        self.client = client

    def __getattr__(self, item):
        return functools.partial(
            getattr(self.client, item),
            content_type='application/json',
            HTTP_CONTENT_TYPE='application/json',
            HTTP_ACCEPT='application/json',
            **self.get_headers()
        )

    def get_headers(self):
        return {}


class AuthenticatedClient(ApiClient):
    def __init__(self, client, user):
        super(AuthenticatedClient, self).__init__(client)
        self.token = Token.objects.create(user=user)

    def get_headers(self):
        return {
            'HTTP_AUTHORIZATION': f'Token {self.token.key}'
        }


@pytest.fixture
def auth_client(client, django_user):
    return AuthenticatedClient(client, django_user)


@pytest.fixture
def api_client(client):
    return ApiClient(client)


@pytest.fixture
def django_user():
    user = User.objects.create(
        username='test',
        first_name='John',
        last_name='Doe',
        email='test@example.com'
    )
    return user


@pytest.fixture
def other_user():
    user = User.objects.create(
        username='test2',
        first_name='Jane',
        last_name='Doe',
        email='test2@example.com'
    )
    return user


@pytest.fixture
def pages(django_user, other_user):
    page1 = Page.objects.create(name='Page 1', created_by=django_user)
    page2 = Page.objects.create(name='Page 2', created_by=other_user)
    return [page1, page2]
