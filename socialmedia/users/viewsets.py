from rest_framework import viewsets

from socialmedia.permissions import ReadOnly
from users.models import User
from users.serializers import PublicUserSerializer


class PublicUserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PublicUserSerializer
    queryset = User.objects.all()
    permission_classes = [ReadOnly]
