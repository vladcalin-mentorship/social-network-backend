from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token

prefix = 'api/v1'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', include('swagger_docs.urls')),

    path(f'{prefix}/login/', obtain_auth_token),
    path(f'{prefix}/users/', include('users.urls')),
    path(f'{prefix}/pages/', include('pages.urls')),
    path(f'{prefix}/posts/', include('posts.urls')),
    *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
]
