from rest_framework import serializers
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView, ListCreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated, SAFE_METHODS

from users.permissions import IsFriendRequestSenderOrReceiver
from users.serializers import FullUserSerializer, CurrentUserSerializer, FriendRequestSerializer, \
    DetailedFriendRequestSerializer


class CreateUserApiView(CreateAPIView):
    serializer_class = FullUserSerializer


class CurrentUserApiView(RetrieveUpdateAPIView):
    serializer_class = CurrentUserSerializer
    permission_classes = (IsAuthenticated,)

    password = serializers.CharField(required=False)

    def get_object(self):
        return self.request.user


class FriendRequestCreateListAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if not self.request:
            return DetailedFriendRequestSerializer
        if self.request.method in SAFE_METHODS:
            return DetailedFriendRequestSerializer
        else:
            return FriendRequestSerializer

    def get_queryset(self):
        return self.request.user.sent_friend_requests.all()


class DeleteFriendRequestAPIView(DestroyAPIView):
    serializer_class = FriendRequestSerializer
    permission_classes = (IsFriendRequestSenderOrReceiver,)

    def get_queryset(self):
        return self.request.user.sent_friend_requests.all()
