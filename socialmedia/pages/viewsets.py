from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS

from pages.models import Page
from pages.serializers import FullPageSerializer, PublicPageSerializer


class PageManipulationPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        # only authenticated users can create
        # all users can see
        if request.method in SAFE_METHODS:
            return True
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        # everybody can read
        if request.method in SAFE_METHODS:
            return True
        # only the owner can change
        return request.user.is_authenticated and obj.created_by == request.user


class PageViewSet(viewsets.ModelViewSet):
    permission_classes = (PageManipulationPermission,)

    def get_queryset(self):
        return Page.objects.all()

    def get_serializer_class(self):
        if not self.request:
            return FullPageSerializer
        if self.request.user.is_authenticated:
            return FullPageSerializer
        else:
            return PublicPageSerializer

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.created_by = self.request.user
        instance.save()
        return instance

    class Meta:
        model = Page
