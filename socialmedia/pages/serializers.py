from rest_framework import serializers

from pages.models import Page
from users.serializers import PublicUserSerializer, FullUserSerializer


class PublicPageSerializer(serializers.ModelSerializer):
    created_by = PublicUserSerializer()

    class Meta:
        model = Page
        fields = ('id', 'name', 'image', 'cover', 'description', 'created_by', 'type')


class FullPageSerializer(serializers.ModelSerializer):
    created_by = FullUserSerializer()

    class Meta:
        model = Page
        fields = '__all__'
