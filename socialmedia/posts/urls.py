from django.urls import path
from rest_framework.routers import DefaultRouter

from posts.views import ListCreatePostApiView, RetrieveUpdateDestroyPostAPIView

urlpatterns = [
    path('', ListCreatePostApiView.as_view(), name='post_list_create'),
    path('<pk>/', RetrieveUpdateDestroyPostAPIView.as_view(), name='post_retrieve_update_destroy'),
]
