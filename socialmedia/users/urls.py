from django.urls import path
from rest_framework.routers import DefaultRouter

from users.views import CreateUserApiView, CurrentUserApiView, FriendRequestCreateListAPIView, \
    DeleteFriendRequestAPIView
from users.viewsets import PublicUserViewSet

router = DefaultRouter()
router.register('public', PublicUserViewSet, basename='public_user')

urlpatterns = [
    *router.urls,
    path('register/', CreateUserApiView.as_view()),
    path('me/', CurrentUserApiView.as_view()),
    path('me/friend_requests/', FriendRequestCreateListAPIView.as_view()),
    path('me/friend_requests/<pk>/', DeleteFriendRequestAPIView.as_view()),
]
