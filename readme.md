# SocialMedia backend

## Installing and starting the project with Python

Prerequisites:

- `python 3.7+`

To install:

```bash
git clone https://gitlab.com/vladcalin-mentorship/social-network-backend.git
cd social-network-backend
pip install -r requirements.txt
make start
```

## Installing and starting the project with Docker

Prerequisited:

- Docker (`docker run --rm hello-world` must work)

```bash
docker run -it --rm --name social-network-backend -p 8000:8000 registry.gitlab.com/vladcalin-mentorship/social-network-backend
```

That command will pull the docker image and run it. It is self contained, but it will lose its data once it is stopped
(because of the `--rm` flag). If you want the data to persist between restarts, you need to remove the `--rm` flag
and start the backend with `docker start social-network-backend`.



## What is this?

This is a pure backend built with Django and Django REST Framework, with SQLite persistence. Its sole purpose is to
offer a backend for developing frontend only applications, as a learning exercise in learning and building frontends.
More details here:
[https://vladcalin.ro/mentorship/frontend-react/social-media](https://vladcalin.ro/mentorship/frontend-react/social-media)

## Documentation

The documentation for the project can be accessed on the backend itself, at `http://localhost:8000/docs/swagger/`. Here
you will find all the available routes, the methods and some quick information about what parameters/data they accept
and what they return.

The API respects the RESTful guidelines, meaning that each resource has its own URL, methods are used to indicate the
desired action (create, retrieve, update or delete... the usual CRUD) and status codes are used to communicate the
result of the action.

## Authentication

Some routes require authentication. Authentication is done via token based authentication. To obtain a token, a request
needs to be made at `/api/v1/login/` with the username and password, and a `token` will be received.

To authenticate further requests, the client needs to send the `Authorization: Token xxxxxxxxxxxxxxxxx` header.


..note::

    The API is meant to be discovered as the frontend application progresses. The tasks are crafted in such a way
    so the person building the frontend will need to work just with some API endpoints at a time, to avoid information
    overload. In real life, APIs are much bigger, and no developer is able to grasp them from the get go.


More details at [https://vladcalin.ro/mentorship/frontend-react/social-media](https://vladcalin.ro/mentorship/frontend-react/social-media)
