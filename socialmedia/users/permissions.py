from rest_framework.permissions import BasePermission


class IsFriendRequestSenderOrReceiver(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.from_user == request.user or obj.to_user == request.user
