FROM python:3.9-slim

RUN apt update && apt install cmake -y
ADD requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
ADD . /code

WORKDIR code

ENV ENV=PRODUCTION

CMD ["make", "start"]