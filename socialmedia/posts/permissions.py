from rest_framework.permissions import BasePermission, SAFE_METHODS


class PostOwnerOrReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        else:
            if obj.posted_by_user:
                return obj.posted_by_user == request.user
            elif obj.posted_by_page:
                return obj.posted_by_page.created_by == request.user

