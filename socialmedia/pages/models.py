import secrets
from os.path import splitext

from django.contrib.auth import get_user_model
from django.db import models


def get_page_image_path(instance, filename):
    _, ext = splitext(filename)
    return f'pages/{secrets.token_urlsafe(20)}{ext}'


class PageType(models.TextChoices):
    BUSINESS = 'business'
    RESTAURANT = 'restaurant'
    TERRACE = 'terrace'
    PUB = 'pub'
    ARTIST = 'artist'
    PUBLIC_FIGURE = 'public_figure'
    BAND = 'band'


class Page(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=get_page_image_path, null=True, blank=True)
    cover = models.ImageField(upload_to=get_page_image_path, null=True, blank=True)
    description = models.CharField(max_length=1000)

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey(
        get_user_model(),
        related_name='pages',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    type = models.CharField(choices=PageType.choices, max_length=30)
